
#[derive(Clone)]
pub struct RkInitial<'a> {
    functions: &'a Functions<'a>,
    k_coefficients: Vec<Vec<f64>>,
    next_iter_coef: Vec<f64>,
    step: f64,
    values: Vec<f64>,
    initial_values: Vec<f64>,
}


pub struct Functions<'a>(&'a Vec<Box<Fn(&[f64]) -> f64>>);

impl<'a> Functions<'a> {
    pub fn new(functions: &'a Vec<Box<Fn(&[f64]) -> f64>>) -> Self {
        Functions(functions)
    }
}

impl<'a> RkInitial<'a> {
    pub fn new(
        functions: &'a Functions<'a>,
        k_coefficients: &[Vec<f64>],
        next_iter_coef: &[f64],
        step: f64,
        values: &[f64],
    ) -> Self {
        RkInitial {
            functions,
            k_coefficients: k_coefficients.to_vec(),
            next_iter_coef: next_iter_coef.to_vec(),
            step,
            values: values.to_vec(),
            initial_values: values.to_vec(),
        }
    }

    pub fn restore_initial_values(&mut self) {
        self.values = self.initial_values.clone();
    }

    pub fn set_step(&mut self, step: f64) {
        self.step = step;
    }

    fn get_k_coef(&mut self) -> Vec<Vec<f64>> {
        let mut result = Vec::new();
        for function in self.functions.0 {
            let mut k = vec![0f64; self.k_coefficients.len()];
            k[0] = function(&self.values);
            for j in 1..self.k_coefficients[0].len() {
                let mut coef_accum = Vec::new();
                for value in self.values.iter().take(self.values.len() - 1) {
                    let mut value_sum = *value;
                    for z in 0..j {
                        value_sum += self.step * self.k_coefficients[j][z + 1] * k[z];
                    }
                    coef_accum.push(value_sum);
                }
                coef_accum
                    .push(self.values.last().unwrap() + self.step * self.k_coefficients[j][0]);
                k[j] = function(&coef_accum);
            }
            result.push(k);
        }
        result
    }
}

impl<'a> Iterator for RkInitial<'a> {
    type Item = Vec<f64>;

    fn next(&mut self) -> Option<Self::Item> {
        let k_coefficients = self.get_k_coef();
        let self_values_len = self.values.len();
        for (i, value) in self.values.iter_mut().enumerate().take(self_values_len - 1) {
            *value += self.step * {
                let mut accum = 0.0;
                for j in 0..self.next_iter_coef.len() {
                    accum += k_coefficients[i][j] * self.next_iter_coef[j];
                }
                accum
            }
        }
        *(self.values.last_mut().unwrap()) += self.step;
        Some(self.values.clone())
    }
}


#[cfg(test)]
mod tests {

    fn f(values: &[f64]) -> f64 {
        values[2] / values[1]
    }

    fn g(values: &[f64]) -> f64 {
        -values[2] / values[0]
    }

    #[test]
    fn epic_fail() {
        use super::RkInitial;
        use super::Functions;
        let step = 0.001;
        let eps = 0.01;
        let f = Box::new(f);
        let g = Box::new(g);
        let functions: Vec<Box<Fn(&[f64]) -> f64>> = vec![f.clone(), g.clone()];
        let functions = Functions::new(&functions);
        let k_coefficients = vec![vec![1f64, 1f64], vec![1f64 / 2f64, 1f64 / 2f64]];
        let next_iter_coef = vec![0f64, 1f64];
        let values = vec![1.0, 1.0, 0.0];
        let rk = &mut RkInitial::new(&functions, &k_coefficients, &next_iter_coef, step, &values);
        for (i, result) in rk.take((1.0 / step) as usize).enumerate() {
            assert!((result[0] - x_precise(i as f64 * step)).abs() < eps);
            assert!((result[1] - y_precise(i as f64 * step)).abs() < eps);
        }
        let step = step / 2.0;
        rk.restore_initial_values();
        rk.set_step(step);
        for (i, result) in rk.take((1.0 / step) as usize).enumerate() {
            assert!((result[0] - x_precise(i as f64 * step)).abs() < eps);
            assert!((result[1] - y_precise(i as f64 * step)).abs() < eps);
        }
    }

    fn x_precise(t: f64) -> f64 {
        use std::f64::consts::E;
        E.powf(t * t / 2f64)
    }

    fn y_precise(t: f64) -> f64 {
        use std::f64::consts::E;
        E.powf(-(t * t) / 2f64)
    }
}
